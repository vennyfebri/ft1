package hari1;

import java.util.Scanner;


public class no8 {
	/*
	 * Seringkali kita melihat data yang dienkripsi terhadap informasi yang bersifat
	 * rahasia. Beragam metode enkripsi telah dikembangkan hingga saat ini. Kali ini
	 * kalian akan ditugaskan untuk membuat metode enkripsi sendiri agar informasi
	 * yang diinput menjadi aman, dengan menggunakan original alfabet yang dirotasi
	 * pada enkripsi tersebut.
	 * 
	 * Constraint : - Original Alfabet : abcdefghijklmnopqrstuvwxyz - Semua enkripsi
	 * hanya menggunakan huruf kecil - Spasi/karakter spesial tidak dianggap
	 * 
	 * Input : string : mengandung data yang belum dienkripsi n : jumlah huruf yang
	 * digunakan untuk merotasi original alfabet (0 <= n <= 100)
	 * 
	 * Example string : ba ca n : 3
	 * 
	 * Output Original Alfabet : abcdefghijklmnopqrstuvwxyz Alfabet yang dirotasi :
	 * defghijklmnopqrstuvwxyzabc Hasil enkripsi : edfd
	 */

	public static void main(String[] args) {
		String p = "ba ca".replace(" ", "");
		int n = 3 ;
		
		String []aP = p.split("");
		String []aAlfabet = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
		
		

	}

}
