package hari1;

import java.util.Scanner;

public class no10 {

	public static void main(String[] args) {
		Scanner bc = new Scanner(System.in);
		
		System.out.print("input n : ");
		String kalimat = bc.nextLine();
		
		String kata = kalimat.replace(" " , "").toLowerCase();
		
		String []aKalimat = kata.split("");
		
		String vokal = "";
		String konsonan = "";
		String others = "";
		
		//memisahkan vokal dan konsonan
		for(int i=0; i<=aKalimat.length-1; i++) {
			if(aKalimat[i].equals("a") || aKalimat[i].equals("i") || aKalimat[i].equals("u") || aKalimat[i].equals("e") || aKalimat[i].equals("o")) {
				vokal = vokal + aKalimat[i];
				
			} else if(aKalimat[i].equals("b") || aKalimat[i].equals("c") || aKalimat[i].equals("d") || aKalimat[i].equals("f") || aKalimat[i].equals("g") ||
					aKalimat[i].equals("h") || aKalimat[i].equals("j") || aKalimat[i].equals("k") || aKalimat[i].equals("l") || aKalimat[i].equals("m") || aKalimat[i].equals("n") ||
					aKalimat[i].equals("p") || aKalimat[i].equals("q") || aKalimat[i].equals("r") || aKalimat[i].equals("s") || aKalimat[i].equals("t") || aKalimat[i].equals("v") ||
					aKalimat[i].equals("w") || aKalimat[i].equals("x") || aKalimat[i].equals("y") || aKalimat[i].equals("z")) {
				konsonan = konsonan + aKalimat[i];
			}else {
				others = others + aKalimat[i];
			}
		}
		
		String []aVokal = vokal.split("");
		String []aKonsonan = konsonan.split("");
		String []aOthers = others.split("");
		
		String simpan="";
		
		
		//sorting
		//vokal
		for (int i = 0; i < aVokal.length; i++) {
			for (int j = 0; j < aVokal.length-1; j++) {
				if(aVokal[j].compareTo(aVokal[j+1])>0) {
					simpan = aVokal[j];
					aVokal[j] = aVokal[j+1];
					aVokal[j+1] = simpan;
				}
			}
		}
		//konsonan
		for (int i = 0; i < aKonsonan.length; i++) {
			for (int j = 0; j < aKonsonan.length-1; j++) {
				if(aKonsonan[j].compareTo(aKonsonan[j+1])>0) {
					simpan = aKonsonan[j];
					aKonsonan[j] = aKonsonan[j+1];
					aKonsonan[j+1] = simpan;
				}
			}
		}
		
		//konsonan
				for (int i = 0; i < aOthers.length; i++) {
					for (int j = 0; j < aOthers.length-1; j++) {
						if(aOthers[j].compareTo(aOthers[j+1])>0) {
							simpan = aOthers[j];
							aOthers[j] = aOthers[j+1];
							aOthers[j+1] = simpan;
						}
					}
				}
		
		
		//cetak
		System.out.print("Huruf vokal : ");
		for(int i=0; i< aVokal.length; i++) {
			System.out.print(aVokal[i]);
		}
		System.out.println();
		
		System.out.print("Huruf Konsonan : ");
		for(int i=0; i< aKonsonan.length; i++) {
			System.out.print(aKonsonan[i]);
		}
		
		System.out.println();
		System.out.print("Others : ");
		for(int i=0; i< aOthers.length; i++) {
			System.out.print(aOthers[i]);
		}
		
		bc.close();

	}

}
